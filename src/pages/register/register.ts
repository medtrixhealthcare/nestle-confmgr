import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CommonService } from '../../service/common/common';
import { IntroPage } from '../intro/intro';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage {

  fullname: string = '';
  email: string = '';
  country: string = '';
  accesscode: string = '';
  countryList: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: CommonService,
    private storage: Storage
  ) {
    this.service.getCountryList().subscribe(
      countryList => {
        if (!countryList.iserror) {
          this.countryList = countryList.result;
        }
      },
      (error) => {
        console.log(error)
      });
  }

  back() {
    this.service.goBack(this.navCtrl);
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad RegisterPage');
  }

  onRegister() {
    if (this.fullname == '') {
      this.service.showAlert('', 'Please enter fullname.');
      return;
    }

    if (this.email == '') {
      this.service.showAlert('', 'Please enter email.');
      return;
    }

    if (!this.ValidateEmail(this.email)) {
      this.service.showAlert('', 'Please enter a valid email.');
      return;
    }

    if (this.country == '') {
      this.service.showAlert('', 'Please select a country.');
      return;
    }

    if (this.accesscode == '') {
      this.service.showAlert('', 'Please enter accesscode.');
      return;
    }

    let data = {
      "fullname": this.fullname,
      "email": this.email,
      "accesscode": this.accesscode,
      "token": "token",
      "country": this.country
    }

    this.service.showLoader();
    this.service.registerUser(data)
      .subscribe(
        userDetail => {
          this.service.hideLoader();
          if (!userDetail.iserror) {
            if (userDetail.result && userDetail.result.iserror) {
              this.service.showAlert('', userDetail.result.message);
              return;
            }

            this.storage.get('RegistrationId').then((val) => {
              if (val) {
                
                let notificationApi = {
                  "userid": userDetail.result[0].userid,
                  "devicetype": "android",
                  "token": val
                }

                this.service.setDeviceTokenNotification(notificationApi)
                  .subscribe((response) => {
                    console.log("Set Device Success : " , response)
                  });
              }
              else{
                console.log("Set Device Failure : ")
              }
            });

            this.navCtrl.setRoot(IntroPage, { isRegistered: true, name: userDetail.result[0].fullname }, {
              animate: true, animation: 'ios-transition', direction: 'forward'
            });
            this.storage.set("userdetail", userDetail.result[0]);
            
          }
        },
        (error) => {
          console.log(error)
        });
  }

  ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true
    }
    return false
  }

}
