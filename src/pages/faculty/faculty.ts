import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-faculty',
  templateUrl: 'faculty.html',
})
export class FacultyPage {

  public facultyPage: any;
  content: object = { name: String, shortdesc: String, photourl: String };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage
  ) { }

  ionViewDidLoad() {
    this.storage.get('updatedJSON').then((val) => {
      this.facultyPage = val.pages.speaker;
      this.content = this.facultyPage.content;
      // console.log(this.facultyPage.title);
    })
    // console.log('ionViewDidLoad FacultyPage');
  }

}
