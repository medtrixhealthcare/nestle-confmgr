import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmailnotePage } from './emailnote';

@NgModule({
  declarations: [
    EmailnotePage,
  ],
  imports: [
    IonicPageModule.forChild(EmailnotePage),
  ],
})
export class EmailnotePageModule {}
