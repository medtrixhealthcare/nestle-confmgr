import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {

  notificationPages: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage
  ) { }

  ionViewWillEnter() {
    // console.log('ionViewDidLoad NotificationPage');
    this.storage.get("Notification").then((val) => {
      if(val) {
        console.log(JSON.stringify(val));
        this.notificationPages = val;
        this.notificationPages.reverse();
      }
      else {
        console.log("no Notifications");
      }
    });
  }

}
