import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { PreregisterPage } from '../preregister/preregister';
import { IntroPage } from '../intro/intro';
import { Storage } from '@ionic/storage';

import { CommonService } from '../../service/common/common';
import { ApiProvider } from '../../providers/api/api';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loader: any;

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public loadingCtrl: LoadingController,
    private service: CommonService,
    private apiServ: ApiProvider
  ) { }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Checking for updates...",
    });
    this.loader.present();
  }
  ionViewDidLoad() {

    setTimeout(() => {
      this.presentLoading();
      this.service.checkForUpdate()
        .then((isUpdated) => {
          isUpdated.subscribe((output) => {
            if (output.result) {
              this.apiServ.getPages().subscribe((updatedPages) => {
                this.storage.set("updatedJSON", updatedPages.result);
                this.loader.dismiss();
                this.redirectToSpecific();
              });
            } else {
              this.loader.dismiss();
              this.redirectToSpecific();
            }
          });
        }, (error) => {
          console.log(error);
        });
    }, 3000);

  }
  redirectToSpecific() {
    this.storage.get('userdetail').then((val) => {
      if (val && val.userid) {
        this.navCtrl.setRoot(IntroPage, {}, {
          animate: true, animation: 'ios-transition', direction: 'forward'
        });
      } else {
        this.navCtrl.push(PreregisterPage, {}, {
          animation: 'ios-transition'
        });
      }
    }).catch((error) => console.log(error));
  }

}
