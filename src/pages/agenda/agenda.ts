import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})
export class AgendaPage {

  public agendaPage: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage
  ) { }

  ionViewDidLoad() {
    this.storage.get('updatedJSON').then((val) => {
      this.agendaPage = val.pages.agenda;
      // console.log(this.agendaPage.title);
    })
    // console.log('ionViewDidLoad AgendaPage');
  }

}
