import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VenuedetailsPage } from './venuedetails';

@NgModule({
  declarations: [
    VenuedetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(VenuedetailsPage),
  ],
})
export class VenuedetailsPageModule {}
