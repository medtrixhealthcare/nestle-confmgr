import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-slides',
  templateUrl: 'slides.html',
})
export class SlidesPage {

  public slidesPage: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage
  ) { }

  ionViewDidLoad() {

    this.storage.get('updatedJSON').then((val) => {
      this.slidesPage = val.pages.slides;
      // console.log(this.slidesPage.title);
    });
    // console.log('ionViewDidLoad SlidesPage');
  }

}
