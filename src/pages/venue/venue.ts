import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageViewerController } from 'ionic-img-viewer';

@IonicPage()
@Component({
  selector: 'page-venue',
  templateUrl: 'venue.html',
})
export class VenuePage {

  public _imageViewerCtrl: ImageViewerController;
  public venuePage: any;
  venue: string = "VENUE DETAILS";
  isAndroid: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public storage: Storage,
    public sanitizer: DomSanitizer,
    public imageViewerCtrl: ImageViewerController
  ) {
    this.isAndroid = platform.is('android');
    this._imageViewerCtrl = imageViewerCtrl;
  }

  

  ionViewDidLoad() {
    this.storage.get('updatedJSON').then((val) => {
      this.venuePage = val.pages.venue;
      console.log(this.venuePage);
    });
    // console.log('ionViewDidLoad VenuePage');
  }

  presentImage(myImage) {
    const imageViewer = this._imageViewerCtrl.create(myImage);
    imageViewer.present();
 
    //setTimeout(() => imageViewer.dismiss(), 1000);
    //imageViewer.onDidDismiss(() => alert('Viewer dismissed'));
  }

  public locationURL(mapUrl) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(mapUrl);
  }

}
