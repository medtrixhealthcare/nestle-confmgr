import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the PollingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-polling',
  templateUrl: 'polling.html',
})
export class PollingPage {

  userId: string = "";
  polloption: string = "";
  isSubmitted:boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public service: ApiProvider
  ) { }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad PollingPage');
  }

  submitPoll(event){

    this.polloption = event.target.innerText;
    this.storage.get('userdetail').then((val) => {
      this.userId = val.userid;

      let pollSubmit = {
        "userid": this.userId,
        "polloption": this.polloption
      }

      this.service.submitingPoll(pollSubmit)
      .subscribe((response) => {
        if (response.result) {
          this.isSubmitted = true;
        }
      });
    });

  }

}
