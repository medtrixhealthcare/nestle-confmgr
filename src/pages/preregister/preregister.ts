import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { RegisterPage } from '../register/register';


@IonicPage()
@Component({
  selector: 'page-preregister',
  templateUrl: 'preregister.html',
})
export class PreregisterPage {

  public preregisterPage: any;
  public unregisterBackButtonAction: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public storage: Storage
  ) { }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  ionViewDidLoad() {
    this.storage.get('updatedJSON').then((val) => {
      this.preregisterPage = val.pages.preregister;
    });
    //console.log('ionViewDidLoad PreregisterPage');
  }

  openRegistrationPage() {
    this.navCtrl.push(RegisterPage, {}, {
      animation: 'ios-transition'
    });
  }

  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }

  private customHandleBackButton(): void {
    // do what you need to do here ...
  }

}
