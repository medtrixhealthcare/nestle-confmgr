import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PreregisterPage } from './preregister';

@NgModule({
  imports: [
    IonicPageModule.forChild(PreregisterPage),
  ],
})
export class PreregisterPageModule {}
