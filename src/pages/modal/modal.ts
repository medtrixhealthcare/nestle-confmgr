import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { NotePage } from '../note/note';

/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  topics: any;
  selectedTopic: string = "";
  notes: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage) { }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ModalPage');

    this.storage.get('updatedJSON').then((val) => {
      this.topics = val.pages.agenda;
    });
  }

  onTopicSelect(event) {
    if (event.target.value) {
      this.selectedTopic = event.target.value;
      // console.log(this.selectedTopic);
    }
  }

  saveNote() {
    let notesArr = [];
    let notes = {
      "topic": this.selectedTopic,
      "notes": this.notes,
      "currTime": this.formatAMPM()
    }
    this.storage.get('newNotes').then((val) => {
      if (val) {
        notesArr = val;
        this.newNote(notesArr, notes);
      } else {
        this.newNote(notesArr, notes);
      }
    });
  }

  newNote(noteAr, note){
    noteAr.push(note);
    this.storage.set('newNotes', noteAr);
    this.navCtrl.setRoot(NotePage);
  }

  formatAMPM() {
    let date: any = new Date()
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

}
