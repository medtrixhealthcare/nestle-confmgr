import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the EventsurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-eventsurvey',
  templateUrl: 'eventsurvey.html',
})
export class EventsurveyPage {
  isSubmitted:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public storage: Storage) {
  }

  ionViewWillLoad() {
    this.storage.get('surveySubmitted').then((val) => {
      if(val) {

        document.getElementById("eventSurvey").style.display = 'none';
        document.getElementById("alreadySubmitted").style.display = 'block';
      }
   });
  }

  ionViewDidLoad() {
    this.storage.get('surveySubmitted').then((val) => {
      if(val) {

        document.getElementById("eventSurvey").style.display = 'none';
        document.getElementById("alreadySubmitted").style.display = 'block';
      }
   });

    console.log('ionViewDidLoad EventsurveyPage');
  }

  iframeLoaded(frame) {
    let iframe : any = document.getElementById("eventSurvey");
    if(iframe.src.toLowerCase().indexOf('formresponse') > -1) {
      this.storage.set('surveySubmitted',true);
    }
  }

}
