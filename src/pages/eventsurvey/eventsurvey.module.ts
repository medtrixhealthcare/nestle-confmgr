import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventsurveyPage } from './eventsurvey';

@NgModule({
  declarations: [
    EventsurveyPage,
  ],
  imports: [
    IonicPageModule.forChild(EventsurveyPage),
  ],
})
export class EventsurveyPageModule {}
