import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ModalPage } from '../modal/modal';
import { ApiProvider } from '../../providers/api/api';


@IonicPage()
@Component({
  selector: 'page-note',
  templateUrl: 'note.html',
})
export class NotePage {

  notes: string = "ADD NOTE";
  allNotes: any;
  noteSelected: Array<string> = [];
  isAndroid: boolean = false;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public storage: Storage,
    public service: ApiProvider
  ) {
    this.isAndroid = platform.is('android');
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad NotePage');

    this.storage.get('newNotes').then((val) => {
      this.allNotes = val;
    });
  }

  modalPage() {
    this.navCtrl.push(ModalPage);
  }

  updateNoteSelected(val, event) {
    if (event.checked) {
      this.noteSelected.push(val);
    } else {
      this.noteSelected.splice(this.noteSelected.indexOf(val), 1);
    }
  }

  emailNotes() {
    var len = this.noteSelected.length;
    if (len !== 0) {
      let notesArry = [];
      for (let i = 0; i < len; i++) {
        let temp = {
          "topic": this.allNotes[this.noteSelected[i]].topic,
          "notes": this.allNotes[this.noteSelected[i]].notes
        }
        notesArry.push(temp);
      }
      this.storage.get('userdetail').then((res) => {
        let data = {
          "username": res.fullname,
          "email": res.email,
          "noteslist": notesArry
        }
        this.service.sendingNotesEmail(data)
          .subscribe((resp) => {
            if (resp.result) {
              this.notes = 'ADD NOTE';
            }
          });
      });
    }

  }

  segmentChanged(event) {
    this.noteSelected = [];
  }

}
