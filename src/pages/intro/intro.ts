import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { CommonService } from '../../service/common/common';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  public unregisterBackButtonAction: any;
  public intoPage: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public service: CommonService,
    public api: ApiProvider,
    public storage: Storage
  ) {
    if (navParams.get('isRegistered')) {
      this.service.showAlert('Welcome ' + navParams.get('name') + ' !', 'You have registered successfully.')
    }
  }

  ionViewDidLoad() {
    this.storage.get('updatedJSON').then((val) => {
      this.intoPage = val.pages.introduction;
    });
    // console.log('ionViewDidLoad IntroPage');
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  ionViewWillLeave() {
    // Unregister the custom back button action for this page
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
      this.customHandleBackButton();
    }, 10);
  }

  private customHandleBackButton(): void {
    // do what you need to do here ...
  }

}
