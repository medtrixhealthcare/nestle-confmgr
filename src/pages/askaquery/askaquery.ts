import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the AskaqueryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-askaquery',
  templateUrl: 'askaquery.html',
})
export class AskaqueryPage {

  speakerDetails: any;
  userId: string = '0';
  topic: string = "";
  query: string = "";
  selectVal: string = "";

  isSubmitted: boolean;

  @ViewChild('dataContainer') dataContainer: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public service: ApiProvider
  ) { }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AskaqueryPage');

    this.storage.get('updatedJSON').then((val) => {
      this.speakerDetails = val.pages.agenda;
    });
  }

  onSpeakerSelect(event) {
    if (event.target.value) {
      this.topic = this.speakerDetails.content.body[event.target.value].Topic;
      this.dataContainer.nativeElement.innerHTML = this.topic;
    } else {
      this.dataContainer.nativeElement.innerHTML = "";
    }
  }

  submitQuery() {
    this.storage.get('userdetail').then((val) => {
      this.userId = val.userid;

      let querySubmit = {
        "userid": this.userId,
        "query": this.query,
        "topic": this.topic
      }

      if (this.query !== "" && this.topic !== "") {
        this.service.submitingQuery(querySubmit)
          .subscribe((response) => {
            if (response.result) {
              this.isSubmitted = true;
              this.dataContainer.nativeElement.innerHTML = "";
              this.query = "";
              this.selectVal = "";
            }
          });
      }
    });
  }

}
