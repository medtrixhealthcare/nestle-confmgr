import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AskaqueryPage } from './askaquery';

@NgModule({
  declarations: [
    AskaqueryPage,
  ],
  imports: [
    IonicPageModule.forChild(AskaqueryPage),
  ],
})
export class AskaqueryPageModule {}
