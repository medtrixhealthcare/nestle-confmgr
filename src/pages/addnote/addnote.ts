import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, } from 'ionic-angular';
import { ModalPage } from '../modal/modal';


@IonicPage()
@Component({
  selector: 'page-addnote',
  templateUrl: 'addnote.html',
})
export class AddnotePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,) {
  }

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddnotePage');
  }

  modalPage(){
    this.navCtrl.push(ModalPage, {
      animation: 'ios-transition'
     });
  }; 



}


