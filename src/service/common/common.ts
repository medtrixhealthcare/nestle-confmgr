import { Injectable } from '@angular/core';
import { LoadingController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';


@Injectable()
export class CommonService {
  loading: any;
  url: string = 'http://mx2.us/NestleCM/service/WebService.aspx';
  postData: object = { command: '', data: {}, service: "UserService" };
  isLoading: boolean = false;

  constructor(
    public loadingCtrl: LoadingController,
    private http: Http,
    public alertCtrl: AlertController,
    public storage: Storage
  ) { }


  public goBack(navCtrl) {
    navCtrl.pop({
      animate: true,
      direction: 'back',
      animation: 'ios-transition',
    });
  }

  public showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    if (!this.isLoading) {
      this.isLoading = true;
      this.loading.present();
    }
  }

  public hideLoader() {
    this.loading.dismiss();
    this.isLoading = false;
  }

  showAlert(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['ok']
    });
    alert.present();
  }


  registerUser(data) {
    let pData = { ...this.postData };
    pData['data'] = data;
    pData['command'] = 'registerUser';
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => response.json())
      .catch(this.handleError);
  }

  getCountryList() {
    let pData = { ...this.postData };
    pData['data'] = "";
    pData['command'] = 'getCountryList';
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => {
        return response.json()
      })
      .catch(this.handleError);
  }

  setDeviceTokenNotification(data) {
    let pData = { ...this.postData };
    pData['data'] = data;
    pData['command'] = 'setDeviceToken';
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => {
        return response.json()
      })
      .catch(this.handleError);
  }

  getNotification(data) {
    let pData = { ...this.postData };
    pData['data'] = data;
    pData['command'] = 'getNotification';
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => {
        return response.json()
      })
      .catch(this.handleError);
  }

  checkForUpdate() {

    return this.storage.get('updatedJSON').then((val) => {
      if (val) {
        let pData = { ...this.postData };
        pData['data'] = val.version;
        pData['command'] = 'checkForUpdate';
        return this.http.post(this.url, JSON.stringify(pData))
          .map(response => response.json())
          .catch(this.handleError);
      } else {
        let pData = { ...this.postData };
        pData['data'] = 0;
        pData['command'] = 'checkForUpdate';
        return this.http.post(this.url, JSON.stringify(pData))
          .map(response => response.json())
          .catch(this.handleError);
      }
    });


    /* let pData = { ...this.postData };
    pData['data'] = version;
    pData['command'] = 'checkForUpdate';
    console.log("pData['data'] : "+pData['data']);
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => response.json())
      .catch(this.handleError); */
  }

  handleError(error: Response) {
    return error.json();
  }

}
