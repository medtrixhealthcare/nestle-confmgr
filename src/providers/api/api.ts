import {Http} from '@angular/http'
import { Injectable } from '@angular/core';
import 'rxjs';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

@Injectable()
export class ApiProvider {


  url:string = 'http://mx2.us/NestleCM/service/WebService.aspx';
  pageData: object = {command:'getUpdatedJson', data:"", service:"UserService"};
  postData: object = { command: '', data: {}, service: "UserService" };


  constructor(public http: Http) {
    console.log('Hello ApiProvider Provider');
  }

  getPages() {
    return this.http.post(this.url, JSON.stringify(this.pageData))
      .map(response => response.json())
      .catch(this.handleError);
  }

  submitingPoll(data) {
    let pData = { ...this.postData };
    pData['data'] = data;
    pData['command'] = 'submitPoll';
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => response.json())
      .catch(this.handleError);
  }

  submitingQuery(data) {
    let pData = { ...this.postData };
    pData['data'] = data;
    pData['command'] = 'submitQuery';
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => response.json())
      .catch(this.handleError);
  }

  sendingNotesEmail(data) {
    let pData = { ...this.postData };
    pData['data'] = data;
    pData['command'] = 'sendNotes';
    return this.http.post(this.url, JSON.stringify(pData))
      .map(response => response.json())
      .catch(this.handleError);
  }


  handleError(error: Response) {
    return error.json();
  }

}
