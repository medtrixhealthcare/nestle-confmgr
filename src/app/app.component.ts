import { Component, ViewChild  } from '@angular/core';
import { Platform, AlertController, Nav, Keyboard  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Push, PushObject, PushOptions} from '@ionic-native/push';

import { FacultyPage } from '../pages/faculty/faculty';
import { VenuePage } from '../pages/venue/venue';
import { AgendaPage } from '../pages/agenda/agenda';
import { HomePage } from '../pages/home/home';
import { PollingPage } from '../pages/polling/polling';
import { NotificationPage } from '../pages/notification/notification';
import { EventsurveyPage } from '../pages/eventsurvey/eventsurvey';
import { NotePage } from '../pages/note/note';
import { SlidesPage } from '../pages/slides/slides';
import { AskaqueryPage } from '../pages/askaquery/askaquery';
import { CommonService } from '../service/common/common';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav : Nav;
  rootPage: any = HomePage;
  alert: any;
  notification: number = 0;

  constructor(
    platform: Platform,
    statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    keyboard : Keyboard, 
    private network: Network, 
    public alertCtrl: AlertController, 
    private push: Push,
    public storage : Storage,
    private service: CommonService
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.pushsetup();
      if (platform.is('android')) {
        statusBar.overlaysWebView(false);
      }
      this.init();
      platform.resume.subscribe(() => {
        this.updateNotification();
      });
    });


  }
 
  pushsetup() {
    const options: PushOptions = {
     android: {
         senderID: '215904315541',
         forceShow: "1"
     },
     ios: {
         alert: 'true',
         badge: true,
         sound: 'false'
     },
     windows: {}
  };

  const pushObject: PushObject = this.push.init(options);

  pushObject.on('notification').subscribe((notification: any) => {
     this.updateNotification();
     
   // if (notification.additionalData.foreground) {
      // this.notification++;
      // let notifyArr = [];
      /* let newNotify = {
        title: 'New notification',
        message: notification.message,
        notify: notification
      } */

      /* this.storage.get('Notification').then((val) => {
        if (val) {
          notifyArr = val;
          notifyArr.unshift(newNotify);
          this.storeMassage(notifyArr);
        } else {
          notifyArr.push(newNotify);
          this.storeMassage(notifyArr);
        }
      }); */
      /* let youralert = this.alertCtrl.create({
        title: 'New notification',
        message: notification.message
      });
      youralert.present(); */
   // }
  });

  pushObject.on('registration').subscribe((registration: any) => {
    // console.log(JSON.stringify(registration.registrationId))
    this.storage.set("RegistrationId", registration.registrationId);
    //do whatever you want with the registration ID
  });

  pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
  }


  showAlert() {
    if (this.network.type && this.network.type.toLowerCase() == 'none') {
      this.alert = this.alertCtrl.create({
        title: 'No Internet',
        subTitle: 'Please check your internet connection.',
        enableBackdropDismiss:false,
        buttons: [
          {
            text: 'Retry',
            handler: () => {
              this.showAlert();
            }
          }
        ]
      });
      this.alert.present();
    }
  }

  updateNotification() {
    this.storage.get('Notification').then((val) => {
      if (val) {
        let index = val.length - 1;
        this.service.getNotification(val[index].id)
          .subscribe(response => {
            if(!response.iserror) {
              if(response.result  && response.result.length > 0) {
                var data = val;
                data = data.concat(response.result)
                this.storage.set('Notification',data);
                if(this.nav.getActive().name == 'NotificationPage') {
                  this.nav.setRoot(this.nav.getActive().component);
                }
              }
            }
           // console.log("getNotification Response onInit : " , response);
          })
      } else {
        this.service.getNotification(0)
          .subscribe(response => {
            //console.log("getNotification Response onInit in else : " , response.result);
            this.storage.set('Notification', response.result);
          })
      }
    });
  }

  init() {
    this.updateNotification();
    this.showAlert();
    this.network.onDisconnect().subscribe(() => {
      this.showAlert();
    });
  }

  storeMassage(arg1){
    this.storage.set("Notification", arg1);
  }

  /* page links */
  agendaPage() {
    this.nav.setRoot(AgendaPage, {
     animation: 'ios-transition'
    })
  }

  facultyPage(){
    this.nav.setRoot(FacultyPage, {
      animation: 'ios-transition'
     })
  }

  venuePage(){
    this.nav.setRoot(VenuePage, {
     animation: 'ios-transition'
    })
  }
 

   pollingPage(){
    this.nav.setRoot(PollingPage, {
      animation: 'ios-transition'
     })
  }

   notificationPage(){
    this.notification = 0;
    this.nav.setRoot(NotificationPage, {
      animation: 'ios-transition'
     })
  }

 eventPage(){
    this.nav.setRoot(EventsurveyPage, {
      animation: 'ios-transition'
     })
  }

  notePage(){
    this.nav.setRoot(NotePage, {
      animation: 'ios-transition'
     })
  }

  askaqueryPage(){
    this.nav.setRoot(AskaqueryPage, {
      animation: 'ios-transition'
     })
    }

  slidesPage(){
    this.nav.setRoot(SlidesPage, {
      animation: 'ios-transition'
     })
  }
 
     

}

