import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { PreregisterPage } from '../pages/preregister/preregister';
import {RegisterPage} from '../pages/register/register';
import {IntroPage} from '../pages/intro/intro';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { HttpClientModule } from '@angular/common/http';
import { CommonService } from '../service/common/common';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';

import { AgendaPage } from '../pages/agenda/agenda';
import { VenuePage } from '../pages/venue/venue';
import { VenuedetailsPage } from '../pages/venuedetails/venuedetails';
import { LocationPage } from '../pages/location/location';
import { PollingPage } from '../pages/polling/polling';
import { NotificationPage } from '../pages/notification/notification';
import { EventsurveyPage } from '../pages/eventsurvey/eventsurvey';
import { NotePage } from '../pages/note/note';
import { EmailnotePage } from '../pages/emailnote/emailnote';
import { AddnotePage } from '../pages/addnote/addnote';
import { FacultyPage } from '../pages/faculty/faculty';
import { Push} from '@ionic-native/push';
import { ApiProvider } from '../providers/api/api';
import { AskaqueryPage } from '../pages/askaquery/askaquery';
import { SlidesPage } from '../pages/slides/slides';
import { ModalPage } from '../pages/modal/modal';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PreregisterPage,
    RegisterPage,
    IntroPage,
    AgendaPage,
     VenuePage,
     VenuedetailsPage,
     LocationPage,
     PollingPage,
     NotificationPage,
    EventsurveyPage,
    NotePage,
    EmailnotePage,
    AddnotePage,
    FacultyPage,
    AskaqueryPage,
    SlidesPage,
    ModalPage
  ],
  imports: [
    BrowserModule,
    IonicImageViewerModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      animation: 'ios-transition'
    }),
    HttpClientModule,
    IonicStorageModule.forRoot({
      name: 'nestle',
        driverOrder: ['sqlite', 'websql', 'indexeddb']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PreregisterPage,
    RegisterPage,
    IntroPage,
    AgendaPage,
     VenuePage,
     VenuedetailsPage,
     LocationPage,
     PollingPage,
     NotificationPage,
    EventsurveyPage,
    NotePage,
    EmailnotePage,
    AddnotePage,
    FacultyPage,
    AskaqueryPage,
    SlidesPage,
    ModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CommonService,
    Network,
    ApiProvider,
    Push

  ]
})
export class AppModule {}
